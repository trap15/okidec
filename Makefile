OBJECTS   = okidec.o
ALLOBJS   = $(OBJECTS) oki6295.o
TARGETS   = oki6295dec
LIBS      = -lm
INCLUDE   = -I.
CFLAGS    = -O3 $(INCLUDE)
LDFLAGS   = $(LIBS) -g
CLEANED   = $(ALLOBJS) $(TARGETS)

.PHONY: all clean

all: $(OBJECTS) $(TARGETS)
%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

oki6295dec: $(OBJECTS) oki6295.o
	$(CC) $(LDFLAGS) $(OBJECTS) oki6295.o -o $@

clean:
	$(RM) $(CLEANED)

