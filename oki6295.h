/*
	OkiDec -- OKI sound chip sample decoder
	OKI6295 support header

Copyright (C) 2012		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#ifndef OKI6295_H_
#define OKI6295_H_

void oki6295_init(void);
void oki6295_args(int argc, const char *argv[]);
size_t oki6295_conv(void *in, void *out);
void oki6295_fini(void);

#endif


