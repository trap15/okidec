/*
	OkiDec -- OKI sound chip sample decoder
	OkiDec general include

Copyright (C) 2012		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#ifndef OKIDEC_H_
#define OKIDEC_H_

typedef struct okidec_t
{
	const char *name;
	const char *argstr;
	const char *copyright;
	void (*init)(void);
	void (*args)(int argc, const char *argv[]);
	size_t (*conv)(void *in, void *out);
	void (*fini)(void);
} okidec_t;

#endif


