/*
	OkiDec -- OKI sound chip sample decoder
	Main

Copyright (C) 2012		Alex Marshall "trap15" <trap15@raidenii.net>

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "okidec.h"

extern okidec_t oki;

int main(int argc, const char *argv[])
{
	FILE *ifp, *ofp;
	int start, end;
	size_t insize;
	size_t samples;
	void *indata;
	void *outdata;

	printf("OkiDec (C)2012 Alex Marshall \"trap15\"%s\n", oki.copyright);
	printf("Decoding for: %s\n", oki.name);
	if(argc < 3) {
		printf(	"Usage:\n"
			"\t%s in.bin out.raw %s\n", argv[0], oki.argstr);
		return EXIT_FAILURE;
	}

	ifp = fopen(argv[1], "rb");
	ofp = fopen(argv[2], "wb");
	if(argc > 3) {
		oki.args(argc-3, argv+3);
	}

	fseek(ifp, 0, SEEK_END);
	insize = ftell(ifp);
	fseek(ifp, start, SEEK_SET);
	indata = malloc(insize);
	fread(indata, insize, 1, ifp);
	fclose(ifp);
	outdata = malloc(insize * 2 * 2);

	oki.init();
	samples = oki.conv(indata, outdata);
	oki.fini();

	fwrite(outdata, samples, 2, ofp);
	fclose(ofp);

	free(indata);
	free(outdata);

	return EXIT_SUCCESS;
}

