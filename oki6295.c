/*
	OkiDec -- OKI sound chip sample decoder
	OKI6295 support

Copyright (C) 2012		Alex Marshall <trap15@raidenii.net>
Copyright (C) 1997		Buffoni Mirko

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "okidec.h"
#include "oki6295.h"

/* OKI ADPCM to PCM conversion courtesy of Buffoni Mirko */

int32_t signal;
int32_t step;
const int8_t index_shift[8] = { -1, -1, -1, -1, 2, 4, 6, 8 };
int diff_lookup[49*16];

static int base = -1;
static int end;
static int start;

okidec_t oki = {
	.name = "OKI MSM6295",
	.argstr = "[-b base] [-s start] [-e end]",
	.copyright = ", (C)1997 Buffoni Mirko",
	.args = oki6295_args,
	.init = oki6295_init,
	.conv = oki6295_conv,
	.fini = oki6295_fini,
};

void oki6295_init(void)
{
	int nib;
	int stepval;
	// nibble to bit map
	static const int8_t nbl2bit[16][4] =
	{
		{ 1, 0, 0, 0}, { 1, 0, 0, 1}, { 1, 0, 1, 0}, { 1, 0, 1, 1},
		{ 1, 1, 0, 0}, { 1, 1, 0, 1}, { 1, 1, 1, 0}, { 1, 1, 1, 1},
		{-1, 0, 0, 0}, {-1, 0, 0, 1}, {-1, 0, 1, 0}, {-1, 0, 1, 1},
		{-1, 1, 0, 0}, {-1, 1, 0, 1}, {-1, 1, 1, 0}, {-1, 1, 1, 1}
	};

	// loop over all possible steps
	for(step = 0; step <= 48; step++)
	{
		// compute the step value
		stepval = floor(16.0 * pow(11.0 / 10.0, (double)step));

		// loop over all nibbles and compute the difference
		for(nib = 0; nib < 16; nib++)
		{
			diff_lookup[step*16 + nib] = nbl2bit[nib][0] *
				(stepval   * nbl2bit[nib][1] +
				 stepval/2 * nbl2bit[nib][2] +
				 stepval/4 * nbl2bit[nib][3] +
				 stepval/8);
		}
	}

	signal = -2;
	step = 0;
}

void oki6295_fini(void)
{
}

void oki6295_args(int argc, const char *argv[])
{
	int i;
	for(i = 0; i < argc; i++) {
		if(argv[i][0] == '-') {
			switch(argv[i][1]) {
				case 'b': // base
					sscanf(argv[i+1], "%X", &base);
					i++;
					break;
				case 's': // start
					sscanf(argv[i+1], "%X", &start);
					i++;
					break;
				case 'e': // end
					sscanf(argv[i+1], "%X", &end);
					i++;
					break;
			}
		}
	}
}

static uint16_t oki6295_clock(int nib)
{
	// update the signal
	signal += diff_lookup[step * 16 + (nib & 15)];

	// clamp to the maximum
	if(signal > 2047)
		signal = 2047;
	else if(signal < -2048)
		signal = -2048;

	// adjust the step size and clamp
	step += index_shift[nib & 7];
	if(step > 48)
		step = 48;
	else if(step < 0)
		step = 0;

	// return the signal
	return signal;
}

size_t oki6295_conv(void *in, void *out)
{
	int i, k;
	size_t samples;
	uint8_t *src = in;
	int16_t *dst = out;
	if(base != -1) {
		start = (src[base+0] << 16) |
			(src[base+1] << 8) |
			(src[base+2] << 0);
		end =	(src[base+3] << 16) |
			(src[base+4] << 8) |
			(src[base+5] << 0);
		start &= 0x3FFFF;
		end &= 0x3FFFF;
		if(base & ~0x3FFFF) {
			start |= (base & ~0x3FFFF);
			end |= (base & ~0x3FFFF);
		}
	}
	samples = (end - start) * 2;
	printf("Start:   $%08X\n", start);
	printf("End:     $%08X\n", end);
	printf("Samples: $%08X\n", samples);
	src += start;
	for(i = 0; i < samples; i++) {
		int nib = src[i>>1];
		if(!(i & 1)) nib >>= 4;
		nib &= 0xF;

		*dst++ = oki6295_clock(nib) * 0x10;
	}
	return samples;
}

